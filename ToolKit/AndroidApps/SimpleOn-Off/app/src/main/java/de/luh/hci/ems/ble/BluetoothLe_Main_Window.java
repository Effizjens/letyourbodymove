/**
 * SimpleOn-Off
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */


package de.luh.hci.ems.ble;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.bluetoothle.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;


import de.luh.hci.ems.commands.EMSModule;
import de.luh.hci.ems.commands.IEMSModule;


public class BluetoothLe_Main_Window extends Activity implements OnTouchListener, Observer {
    private Button buttonRightOn;
    private Button buttonLeftOn;
    private RadioGroup radiosLeft;
    private RadioGroup radiosRight;

    private String greyColor = "#ffeaf6ff";

    private String configFileName = "config.txt";
    private File configFile;


    private IEMSModule emsCommandManager;

    private String deviceName = "EMS17BM";

    private static final int REQUEST_ENABLE_BT = 1;

    AlertDialog.Builder alert;

    private void writeDeviceNameToConfigFile() {
        try {
            FileWriter fileWriter = new FileWriter(configFile);
            fileWriter.write(deviceName + "\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_le__main__window);

        // Use this check to determine whether BLE is supported on the device.
        // Then you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }

        configFile = new File(this.getFilesDir(), configFileName);

        if (configFile.exists()) {
            try {
                FileReader reader = new FileReader(configFile);
                BufferedReader bufferedReader = new BufferedReader(reader);
                deviceName = bufferedReader.readLine();
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            writeDeviceNameToConfigFile();
        }

        emsCommandManager = new EMSModule((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE), deviceName);
        emsCommandManager.getBluetoothLEConnector().addObserver(this);
        emsCommandManager.connect();

        buttonRightOn = (Button) findViewById(R.id.buttonRight);
        buttonLeftOn = (Button) findViewById(R.id.buttonLeft);

        buttonRightOn.setOnTouchListener(this);
        buttonLeftOn.setOnTouchListener(this);




        alert = new AlertDialog.Builder(this);

        alert.setTitle("Enter device name");
        alert.setMessage("Enter the name of the device you wish to connect.");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);

        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                deviceName = input.getText().toString();
                emsCommandManager.setDeviceName(deviceName);
                writeDeviceNameToConfigFile();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.

                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bluetooth_le__main__window, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (deviceName != null) {
            menu.getItem(1).setTitle("Current Device: " + deviceName);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_connect) {
            emsCommandManager.connect();
            return true;
        }
        if (id == R.id.action_settings) {
            alert.show();
        }
        if (id == R.id.action_disconnect) {
            emsCommandManager.disconnect();
        }


        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == buttonRightOn) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                emsCommandManager.startCommand(1);
                buttonRightOn.setBackgroundColor(Color.RED);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {

                emsCommandManager.stopCommand(1);
                buttonRightOn.setBackgroundColor(Color.GREEN);
            }

        } else if (v == buttonLeftOn) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                emsCommandManager.startCommand(0);
                buttonLeftOn.setBackgroundColor(Color.RED);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {

                emsCommandManager.stopCommand(0);
                buttonLeftOn.setBackgroundColor(Color.GREEN);
            }

        }

        v.performClick();
        return false;
    }

    @Override
    public void update(Observable observable, Object data) {
        final IEMSBluetoothLEService bleConnector = emsCommandManager.getBluetoothLEConnector();
        if (data == bleConnector) {
            System.out.println("Worked: " + bleConnector.isConnected());

            this.runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       if (bleConnector.isConnected()) {
                           buttonLeftOn.setEnabled(true);
                           buttonLeftOn.setBackgroundColor(Color.GREEN);
                           buttonRightOn.setEnabled(true);
                           buttonRightOn.setBackgroundColor(Color.GREEN);
                           buttonLeftOn.invalidate();
                           buttonRightOn.invalidate();
                       } else {
                           buttonLeftOn.setEnabled(false);
                           buttonLeftOn.setBackgroundColor(Color.parseColor(greyColor));
                           buttonRightOn.setEnabled(false);
                           buttonRightOn.setBackgroundColor(Color.parseColor(greyColor));
                           buttonLeftOn.invalidate();
                           buttonRightOn.invalidate();
                       }
                   }
               }
            );

        }
    }
}