/**
 * SimpleOn-Off
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */

package de.luh.hci.ems.ble;

import java.util.Observer;

/**
 * Created by Tim Dünte on 19.10.2015.
 */
public interface IEMSBluetoothLEService {

    public boolean isConnected();

    public void connectTo(String deviceName);

    public void sendMessageToEMSDevice(String message);

    public void disconnect();

    public void addObserver(Observer observer);
}
