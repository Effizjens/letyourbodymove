/**
 * Multiple Devices
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */


package de.luh.hci.ems.ble;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.bluetoothle.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.luh.hci.ems.commands.EMSBluetoothLEService;
import de.luh.hci.ems.searchandshow.SearchAndShowEMSDevicesView;




public class Multiple_EMSModules_Activity extends Activity implements OnTouchListener, Observer {

    private static boolean ADMIN = true;
    public static final ArrayList<EMSModule> emsModules = new ArrayList<EMSModule>();

    private static final String LEFT = "left";
    private static final String RIGHT = "right";

    private SearchAndShowEMSDevicesView searchAndShowEMSDevicesView;
    private EditText input;
    private boolean isRunningudpBridge = false;

    AlertDialog.Builder alert;

    //BT stuff
    private BluetoothManager bluetoothManager;
    EMSBluetoothLEService bleConnector;

    private Multiple_EMSModules_Activity multiple_emsModules_activity;



    public void createModuleView(String deviceName,  String channel ){

        LinearLayout cont  = (LinearLayout)findViewById(R.id.EMSModules);

        // Add layout
        LayoutInflater  mInflater=(LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout subButtons = (LinearLayout) mInflater.inflate(R.layout.activity_sublayout_button, cont, false);
        subButtons.setTag(deviceName + channel);

        // Add radio buttons
        RadioGroup radioButtonGrpup = (RadioGroup) subButtons.findViewById(R.id.radioButtonGroup);
        radioButtonGrpup.setOnCheckedChangeListener(radiosListener);

        // Add activation buttons
        Button activateButton = (Button) subButtons.findViewById(R.id.activateButton);
        activateButton.setText(deviceName + " " + channel);
        activateButton.setEnabled(true);
        activateButton.setBackgroundColor(Color.GREEN);
        activateButton.setOnTouchListener(this);

        // Add seekBar
        SeekBar seekBar = (SeekBar) subButtons.findViewById(R.id.seekBar);
        seekBar.setLeft(0);
        seekBar.setRight(100);
        seekBar.setProgress(100);

        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        LinearLayout.LayoutParams paramsLy = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        cont.addView(subButtons, paramsLy);

    }
    public void createModuleView(String deviceName  ){

        createModuleView(deviceName, LEFT);
        createModuleView(deviceName, RIGHT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("create", "created");
        this.bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If
        // not displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        bleConnector = EMSBluetoothLEService.getInstance(mBluetoothAdapter);

        // Use this check to determine whether BLE is supported on the device.
        // Then
        // you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }
        setContentView(R.layout.activity_multiple_devices);
        multiple_emsModules_activity = this;



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bluetooth_le__main__window, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        int i =0;
        for (Iterator<EMSModule> it = emsModules.iterator(); it.hasNext(); i++ ) {

            boolean hasItem = false;
            String itString =  it.next().getDeviceName();
            int j =0;
            for (; j < menu.size(); j++){

                MenuItem mi =  menu.getItem(j);
                if ( mi!= null && mi.getTitle().equals(itString)){
                    hasItem = true;
                    break;
                }
            }
            if (!hasItem && emsModules.get(i)!=null&& emsModules.get(i).isConnected() ) {
                menu.add(itString);
            }else{
                if(emsModules.get(i)==null || !emsModules.get(i).isConnected()){
                    menu.removeItem(j);
                }
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    private AlertDialog.Builder getNewAlertDialog() {
        AlertDialog.Builder alert;
        //Creation of an AlertDialog
        alert = new AlertDialog.Builder(this);

        if(ADMIN){
            alert.setTitle("Select the device");
            alert.setMessage("Select the name of the device you wish to connect.");
        }else{
            alert.setTitle("Enter device name");
            alert.setMessage("Enter the name of the device you wish to connect.");
        }


        searchAndShowEMSDevicesView = new SearchAndShowEMSDevicesView(alert.getContext(), null, true);
        searchAndShowEMSDevicesView.setBLEService(bleConnector);


        input = new EditText(this);
        input.setText("");

        if(ADMIN) {
            alert.setView(searchAndShowEMSDevicesView);
        }else{
            // Set an EditText view to get user input

            alert.setView(input);
        }


        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(ADMIN) {
                    for (String deviceName : searchAndShowEMSDevicesView.getNamesOfSelectedItems()) {
                        boolean alreadyInList = false;
                        for(EMSModule emsModule : emsModules){
                            if(emsModule.getDeviceName().equals(deviceName)){
                                alreadyInList = true;
                            }
                        }

                        if(!alreadyInList) {
                            EMSModule emsModule = new EMSModule(bleConnector, deviceName);
                            emsModule.addObserver(multiple_emsModules_activity);
                            emsModule.connect();

                            emsModules.add(emsModule);

                            createModuleView(deviceName);
                        }
                    }

                }

                dialog.dismiss();
                searchAndShowEMSDevicesView.stopScanning();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                dialog.cancel();
                searchAndShowEMSDevicesView.stopScanning();
            }
        });

        return alert;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.reconnectNonConnected) {

            for(EMSModule emsModule: emsModules){
                if(emsModule.isConnected()){
                    emsModule.disconnect();
                }
                emsModule.connect();
            }
            //bleConnector.scanLeDevice(true);
            return true;
        }

        if (id == R.id.chooseDevices) {
            alert = getNewAlertDialog();
            alert.show();
            return true;
        }

        if (id == R.id.action_disconnect) {
            Log.w("Main_Acti", "disconnecing");
            for ( EMSModule emsM : emsModules){

                Log.w ("Bluetooth", "disconnecing in main activity "+ emsM.getDeviceName());
                emsM.disconnect();

            }

            return true;
        }

        if (id == R.id.clearScreen) {

            for(EMSModule emsModule: emsModules){
                emsModule.disconnect();
            }
            emsModules.clear();
            LinearLayout cont  = (LinearLayout)findViewById(R.id.EMSModules);
            cont.removeAllViews();
            return true;

        }
        if (id == R.id.udpBridge) {



           if(!isRunningudpBridge)
           {
               isRunningudpBridge=true;
               UDPbridge.getInstance().runUdpServer();
               item.setTitle("Stop UDP Bridge");

           }else{

               UDPbridge.getInstance().stopUdpServer();
               isRunningudpBridge=false;
               item.setTitle("Start UDP Bridge");

           }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public String getModuleNameFromTag (String tag){

        String deviceName = "";

        if (tag.contains(LEFT)){
            deviceName = tag.replace(LEFT,"");
        }

        if (tag.contains(RIGHT)){
            deviceName = tag.replace(RIGHT,"");
        }

        return  deviceName;

    }

    public int getModuleChannleFromTag ( String tag){
        int channle = -1;

        if (tag.contains(LEFT)){
            channle = 0;
        }

        if (tag.contains(RIGHT)){
            channle  = 1;
        }

        return  channle;

    }

    public int getModuleIndexByName(String deviceName){
        int index =-1;
        int i=0;

        for ( EMSModule m : emsModules){
            if(m.getDeviceName().equalsIgnoreCase(deviceName)){
                index = i;
                break;
            }
            i++;
        }
        return index;
    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {


        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


            String tag = (String) ((LinearLayout) seekBar.getParent().getParent()).getTag();
            String deviceName = getModuleNameFromTag(tag);
            int channel = getModuleChannleFromTag(tag);
            int deviceIndex = getModuleIndexByName(deviceName);

           if (tag ==""|| deviceName =="" || channel ==-1 || deviceIndex ==-1){
               System.err.println(" Wrong convertion: tag " + tag + " deviceName " + deviceName + " channel " + channel + " decieIndex " + deviceIndex);
           }

            // Change intensity of channel 0
            emsModules.get(deviceIndex).setMAXINTENSITY(progress, channel);
            emsModules.get(deviceIndex).setIntensity(progress, channel);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };




    private RadioGroup.OnCheckedChangeListener radiosListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            String tag = (String) ((LinearLayout) group.getParent().getParent()).getTag();
            String deviceName = getModuleNameFromTag(tag);
            int channel = getModuleChannleFromTag(tag);
            int deviceIndex = getModuleIndexByName(deviceName);

            if (tag ==""|| deviceName =="" || channel ==-1 || deviceIndex ==-1){
                System.err.println(" Wrong convertion OnCheckedChangeListener: tag " +tag+ " deviceName " +deviceName+ " channel " +channel+ " decieIndex "+ deviceIndex );
            }

            switch (checkedId) {

                case R.id.radioButtonNormal:
                    emsModules.get(deviceIndex).setPattern(0, channel);
                    break;
               /* case R.id.radioButtonTriangle:
                    emsModules.get(deviceIndex).setPattern(1, channel);
                    break;
                case R.id.radioButtonRect:
                    emsModules.get(deviceIndex).setPattern(2, channel);
                    break;
                case R.id.radioButtonSine:
                    emsModules.get(deviceIndex).setPattern(3, channel);
                    break;
                case R.id.radioButtonIncreaseThenConstant:
                    emsModules.get(deviceIndex).setPattern(4, channel);
                    break;
                    */
                case R.id.radioButtonTextCommand:
                    emsModules.get(deviceIndex).setPattern(5, channel);
                    break;

            }
        }
    };


        @Override
        public boolean onTouch(View v, MotionEvent event) {

            LinearLayout buttonContiner = (LinearLayout) v.getParent();
            String tag = (String) ((LinearLayout) v.getParent()).getTag();
            String deviceName = getModuleNameFromTag(tag);
            int channel = getModuleChannleFromTag(tag);
            int deviceIndex = getModuleIndexByName(deviceName);

            if (tag ==""|| deviceName =="" || channel ==-1 || deviceIndex ==-1){
                System.out.println(" Wrong convertion onTouch: tag " + tag + " deviceName " + deviceName + " channel " + channel + " decieIndex " + deviceIndex);
            }

           // System.out.println(" Wrong convertion onTouch: tag " +tag+ " deviceName " +deviceName+ " channel " +channel+ " decieIndex "+ deviceIndex );

            Log.i("OnTouch", "onTouch");
            if(v.isEnabled()) {
                Log.i("OnTouch", "v enabled");
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (emsModules.get(deviceIndex).getPattern(1) == 5) {
                        EditText tf = (EditText) buttonContiner.findViewById(R.id.emsCommand);
                        String mes = tf.getText().toString();

                        Log.w("Mes String ", mes);
                        if (mes != "" && mes != " ") {
                            emsModules.get(deviceIndex).sendMessageToBoard(mes);
                        }
                    } else {
                        emsModules.get(deviceIndex).startCommandC(channel);
                    }

                    v.setBackgroundColor(Color.RED);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (emsModules.get(deviceIndex).getPattern(1) == 5) {
                    } else {

                        emsModules.get(deviceIndex).stopCommand(channel);
                    }
                    v.setBackgroundColor(Color.GREEN);
                }
                v.performClick();
            }

        return false;
    }


    public String getModuleNameFromObserver(EMSBluetoothLEService data){

      String deviceName ="";

        for ( EMSModule m : emsModules){
            if(m.getBluetoothLEConnector() == data){
                deviceName = m.getDeviceName();
                break;
            }
        }
        return deviceName;
    }

    private ArrayList<View> getAllChildren(View v) {

        if (!(v instanceof ViewGroup)) {
            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            return viewArrayList;
        }

        ArrayList<View> result = new ArrayList<View>();

        ViewGroup viewGroup = (ViewGroup) v;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {

            View child = viewGroup.getChildAt(i);

            ArrayList<View> viewArrayList = new ArrayList<View>();
            viewArrayList.add(v);
            viewArrayList.addAll(getAllChildren(child));

            result.addAll(viewArrayList);
        }
        return result;
    }

    public List<View> findViewByTag (String tag){
        ArrayList<View> views= new ArrayList<View>();
        LinearLayout cont  = (LinearLayout)findViewById(R.id.EMSModules);
       for(int i = 0; i<  cont.getChildCount(); i++){
           View view = cont.getChildAt(i);
           System.out.println(view.getTag() + ";" + view.getId());

           if(view.getTag().toString().contains(tag))
              views.add( view);
       }
       /* ArrayList<View>  vs = cont.getChil getAllChildren(findViewById(R.id.EMSModules));



        for (View v : vs){
            System.out.println(v.getTag() + ";" + v.getId());
            if ( v.getTag().toString().contains(tag)){
                return v;
            }
        }*/
        return views;
    }

    @Override
    public void update(Observable observable, Object data) {
       /* Log.w("Update", "!!! In update!!! " + emsModules.size());
        for (EMSModule emsM : emsModules) {
            if (emsM != null && !emsM.isConnected()) {
                Log.w("Update", " Model is not connected " + emsM.getDeviceName() + "  ");
            }
        }*/

      /*  String deviceName = getModuleNameFromObserver((EMSBluetoothLEService) data);

        if (deviceName == "") {
            Log.w("Update", "  Wrong convertion onTouch: deviceName " + deviceName + "  ");
        }

        if (deviceName != "") {
            final int deviceIndex = getModuleIndexByName(deviceName);

            if (deviceName == "" || deviceIndex == -1) {
                Log.w("Update", " Wrong convertion onTouch: deviceName " + deviceName + " decieIndex " + deviceIndex);
            }
        }*/

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (EMSModule emsM : emsModules) {
                    if (emsM != null) {
                        Log.i("Observer", "Update called.");
                        if(emsM.isConnected()){
                            String deviceName = emsM.getDeviceName();
                            for(View view: findViewByTag(deviceName)){
                                View buttonView = view.findViewById(R.id.activateButton);
                                buttonView.setEnabled(true);
                                buttonView.setBackgroundColor(Color.GREEN);
                            }

                        }else{
                            String deviceName = emsM.getDeviceName();
                            for(View view: findViewByTag(deviceName)){
                                View buttonView = view.findViewById(R.id.activateButton);
                                buttonView.setEnabled(false);
                                buttonView.setBackgroundColor(Color.GRAY);
                            }

                        }

                    }
                }
            }
        });

    }
    @Override
    protected void onDestroy() {

        if(UDPbridge.getInstance()!=null)
            UDPbridge.getInstance().stopUdpServer();

        super.onDestroy();

    }

}