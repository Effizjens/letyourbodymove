package de.luh.hci.ems.ble;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;



/**
 * Created by pfeiffer on 28.03.18.
 */
public class UDPbridge {
    
    private static final String LOG_TAG = "UDP";

    public static final int PORT = 5005;

    private static UDPbridge instance;

    public static UDPbridge getInstance() {
        if(instance == null) {
            instance = new UDPbridge();
        }

        return instance;
    }


    private void paseMessage(String m){


        String moduleName ="";
        String command ="";
        int channel = 0;
        int intensity = 100;
        int time = 0;
        if (m.startsWith("EMS")&& m.length()> 6){


            moduleName = m.substring(0,7);


            EMSModule module = getEmsModuleByName(moduleName);
            if(module != null) {

                command = m.substring(7);

                if (command.length() > 1) {
                    if (command.indexOf('C') != -1) {
                        channel = getNumberOfString(command.substring(command.indexOf('C') + 1));
                    }

                    if (command.indexOf('I') != -1) {
                        intensity = getNumberOfString(command.substring(command.indexOf('I') + 1));
                    }

                    if (command.indexOf('T') != -1) {
                        time = getNumberOfString(command.substring(command.indexOf('T') + 1));
                    }

                    if (module != null &&
                        channel == 0 && channel == 1 && 
                        intensity >= 0 &&
                        time >= 0) {

                        module.startCommandCIT(channel, intensity, time);
                        Log.i(LOG_TAG, "sending: Module Name " + moduleName + " command " + command + " channel " + channel + " intensity " + intensity + " time " + time);
                    }
                }
            }


        }

        //Log.i(LOG_TAG, " 2 received: Module Name" + moduleName + " command " + command + " channel " +channel+" intensity " +intensity+" time " +time);

    }


    private int getNumberOfString(String value){
        String number = "";
        if(value.length()<1){
            Log.e(LOG_TAG, "Error parsing string to number. There is no character in the string. The string is empty.");
            return -1;
        }
        for(int i = 0;i<value.length();i++){
            char c=value.charAt(i);
            if(Character.isDigit(c)){
                number+=c;

            }else{
                if(number!="")
                    return Integer.parseInt(number);
                else{
                    Log.e(LOG_TAG, "Error parsing string to number. Could not get a number from string: "+value);
                    return -1;
                }
            }
        }

        Log.i(LOG_TAG, "ount received: number " + value);
        if(number!="")
            return Integer.parseInt(number);
        else
            return -1;
    }

    private EMSModule getEmsModuleByName(String moduleName) {
        for(EMSModule module : Multiple_EMSModules_Activity.emsModules) {
            if (moduleName.equals(module.getDeviceName()))
                return module;
        }
        return null;
    }

    private AsyncTask<Void, Void, Void> async;
    private boolean serverActive = false;

    public void runUdpServer()
    {
        if(serverActive)
            return;

        serverActive = true;

        async = new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... params)
            {
                byte[] lMsg = new byte[4096];
                DatagramPacket dp = new DatagramPacket(lMsg, lMsg.length);
                DatagramSocket ds = null;

                Log.d(LOG_TAG, "background task started");

                try
                {
                    ds = new DatagramSocket(PORT);

                    while(serverActive)
                    {
                        ds.receive(dp);

                        byte[] data = dp.getData();
                        String s = new String(data, 0, dp.getLength());
                        if(s.length() > 0) {
                            Log.i(LOG_TAG, "received: " + s);
                            paseMessage(s);
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    if (ds != null)
                    {
                        ds.close();
                    }
                }

                return null;
            }
        };

        if (Build.VERSION.SDK_INT >= 11) async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else async.execute();
    }

    public void stopUdpServer()
    {
        serverActive = false;
    }
}
