/**
 * SmartwatchTrigger
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */

package de.luh.hci.ems_smartwatch_example;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import de.luh.hci.ems.commands.EMSModule;


public class SimpleButtonActivity extends Activity implements View.OnTouchListener {

    private Button button;

    private static SimpleButtonActivity act;

    EMSModule emsModule;
    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        act = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_button);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                button = (Button) stub.findViewById(R.id.button);
                button.setOnTouchListener(act);
            }
        });

        String deviceName = "";
        emsModule = new EMSModule((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE), deviceName);
       // emsModule.getBluetoothLEConnector().addObserver(this);

        alert = new AlertDialog.Builder(this);

        alert.setTitle("Enter device name");
        //alert.setMessage("Enter the name of the device you wish to connect.");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setText("EMS26GF");
        input.setTextColor(Color.BLACK);



        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                emsModule.setDeviceName(input.getText().toString());
                emsModule.connect();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
        alert.show();
        emsModule.setMAX_INTENSITY(100,0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        System.out.println("pushed");
        if (v == button) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                emsModule.sendMessageToBoard("I100C0T2500G");
                button.setBackgroundColor(Color.RED);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                emsModule.sendMessageToBoard("I0C0T0G");
                button.setBackgroundColor(Color.GREEN);
            }
        }
        v.performClick();
        return false;
    }
}